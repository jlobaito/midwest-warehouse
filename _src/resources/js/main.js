// When you change APPName, be sure to update it in mylibs/util.js
// @see http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
var APPNAME = {
  
  initSlider: function() {
    $('.flexslider').flexslider({
      animation: "slide"
    });
  },
  
  // Initializers
  common: {
    init: function() { 
      
    },
    finalize: function() {
      
    }
  },
  
  has_slider: {
    init: function() { 
      APPNAME.initSlider();
    },
    finalize: function() { 
      
    }
  }
};

UTIL = {
  fire: function( func,funcname, args ) {
    var namespace = APPNAME;  // indicate your obj literal namespace here
 
    funcname = ( funcname === undefined ) ? 'init' : funcname;
    if ( func !== '' && namespace[ func ] && typeof namespace[ func ][ funcname ] == 'function' ) {
      namespace[ func ][ funcname ]( args );
    }
  },
  loadEvents: function() {
    var bodyId = document.body.id;
 
    // hit up common first.
    UTIL.fire( 'common' );
 
    // do all the classes too.
    $.each( document.body.className.split( /\s+/ ), function( i, classnm ) {
      UTIL.fire( classnm );
      UTIL.fire( classnm, bodyId );
    });
    UTIL.fire( 'common', 'finalize' );
  }
};

$(document).ready(UTIL.loadEvents);

/* Youtube API */
/*
var tag = document.createElement('script');
  tag.src = "http://www.youtube.com/player_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  var player;
  function onYouTubePlayerAPIReady() {
    var $m = $("#movie_player");
    player = new YT.Player('movie_player', {
      playerVars: { 'autoplay': 1, 'controls': 0,'autohide':1,'wmode':'opaque', 'loop': 1, 'rel':0, 'showinfo':0, 'fs':0,'playlist':$m.data('video') },
      videoId: $m.data("video"),
      events: {
        'onReady': onPlayerReady}
      });
  }
*/

  /* Init */
 

// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header').outerHeight();
    console.log(navbarHeight);

$(window).scroll(function(event){
    didScroll = true;
});

    function iframeEmbed(selector){
      var elem = $(selector);
      elem.wrap( "<div class='video-container'></div>" );
    }

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();
    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    
    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('header').removeClass('nav-down').addClass('nav-up').css('top',-navbarHeight);
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('header').removeClass('nav-up').addClass('nav-down').css('top','0');
        }
    }
    
    lastScrollTop = st;
}


    $(document).ready(function(){
        $("#mobile-menu").menumaker({
            format: "multitoggle"
        });
        $('li.has-sub > a').attr('href',"");
    });

$('.imgs img:first-child').addClass('first');


$(document).ready(function(){
  $(".fancybox").fancybox();


  iframeEmbed('iframe');

/*  $(".lazy-load img").lazyload({
    effect : "fadeIn"
  });*/
  $('.gallery-page .mix').each(function(){
    var className = $(this).parent('span').data('album'),
        link = $(this).parent('span').data('link');
    $(this).addClass(className).attr('data-link', link);
  });
  $('.gallery-page .gal > *').unwrap()


  $('.gallery-row').mixItUp({
    layout: {
      display: 'block'
    },
    load: {
      sort: 'random'
    }
  });
  
  var $filterSelect = $('#FilterSelect'),
      $container = $('.gallery-row');

  $filterSelect.on('change', function(){
    $container.mixItUp('filter', this.value);
  });



  $('.mix').click(function(){
    var desc = $(this).data('description'),
        link = $(this).data('link'),
        product = $(this).data('product'),
        picture = $(this).data('picture');

    $('#myModal #gal-pic').attr('src' , picture);
    $('#myModal #gal-name, #myModal #gal-prod').html(product);
    $('#myModal #gal-desc').html(desc);
    $('#myModal #prodLink').attr('href' , 'products/' + link);

  });
  $('.B_crumbBox li:nth-child(2)').hide();

  $('.page-builder-content table').addClass('table').wrap('<div class="table-responsive"></div>');

})





