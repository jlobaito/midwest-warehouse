<?php
$count = 0;
$chunk = isset($chunk) ? $chunk : '';
$criteria = '';
$parent = isset($parent) ? (integer) $parent : 0; // Current page parent
$container = isset($container) ? (integer) $container : 0; // Container to check for matching parent of resource. IE: 5 - products
$id = isset($id) ? (integer) $id : 0; // current page id
$childChunk = isset($childChunk) ? $childChunk : 'showChildren';
$siblingChunk = isset($siblingChunk) ? $siblingChunk : 'showSiblings';

if ($id > 0) {
	$criteria = array(
		'parent' => $id,
		'deleted' => false,
		'published' => true,
	);
	$count = $modx->getCount('modResource', $criteria);
}

if($container == $parent){
	if($count > 0){
		$chunk = $childChunk ;
	}
} else {
	$chunk =  $siblingChunk ;
}

if($chunk != ""){
	return $chunk;
}